
app.controller 'CompaniesCtrl', ($rootScope,$scope,$API,$timeout) ->


	$scope.links =

		data: [
			{
				"slug": "contact"
				"page": "form"
				"params": {"form":"company-contact"}
				"title": "Datos de contacto"
			},{
				"slug": "payment"
				"page": "form"
				"params": {"form":"company-payment"}
				"title": "Datos de recepción de dinero y pago"
			},{
				"slug": "features"
				"page": "form"
				"params": {"form":"company-features"}
				"title": "Descripción empresa"
			},{
				"slug": "location"
				"page": "form"
				"params": {"form":"company-location"}
				"title": "Geolocalización de tus productos o servicios"
			},{
				"slug": "social"
				"page": "form"
				"params": {"form":"company-social"}
				"title": "Web y redes sociales"
			},{
				"slug": "certification"
				"page": "company-certification"
				"title": "Pagar o renovar servicio de certificación"
			},{
				"slug": "documents"
				"page": "company-documents"
				"title": "Gestión de documentos"
			}
		]

		getCompletedPercent: ->

			items = this.data.length
			completed = 0

			for d in this.data
				if d.completed
					completed++

			percent = parseInt( completed * 100 / items )

			return percent


	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.companies'

	$scope.company = undefined

	if $rootScope.isAvailableUploadCertification
		$scope.documents.available = true

	if $scope.user.currentCompanyId	
		$API.getCompany({id:$scope.user.currentCompanyId}).then (res) ->
			$scope.company = res.data

			for link in $scope.links.data
				link.completed = res.data.completedProfile[link.slug]




app.controller 'CompanyCertificationCtrl', ($rootScope,$scope,$API,$TRACK) ->

	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.companies'

	$scope.certif =
		locked: true
		qSteps: 3
		currentStep: 1
		steps: ["amount","method","success"]
		go: (step) ->
			this.currentStep = step
			$TRACK({type:"step",value:step})
		next: ->
			this.go(this.currentStep+1)
		back: ->
			this.go(this.currentStep-1)
			$TRACK({type:"back"})
		data: {}

	if $rootScope.isAvailableUploadCertification
		$scope.certif.locked = false



app.controller 'CompanyDocumentsCtrl', ($rootScope,$scope,$API,$timeout) ->

	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.companies'

	$scope.documents =
		available: true
		form: $rootScope.forms['company-documents']
		data: [
			{title:'Certificado de vigencia de sociedad'},
			{title:'Poder de representante legal'},
			{title:'Escritura de la empresa'}
		]

	$scope.documents.form.callback = ->
		$timeout ->
			$scope.documents.success = true
			$rootScope.isAvailableUploadCertification = true
		,2000

	if $rootScope.isAvailableUploadCertification
		$scope.documents.available = true

	return




