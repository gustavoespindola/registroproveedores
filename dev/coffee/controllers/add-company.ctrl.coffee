
app.controller 'AddCompanyCtrl', ($scope,$rootScope,$timeout,$TRACK) ->

	$scope.addCompany =
		qSteps: 4
		currentStep: 0
		steps: ["sii","contactdata","payment","product","region","social"]
		go: (step) ->
			$TRACK({type:"step",value:step})
			this.currentStep    = step

		forms:
			sii:         $rootScope.forms['company-login-sii']
			contactdata: $rootScope.forms['company-contact']
			payment:     $rootScope.forms['company-payment']
			product:     $rootScope.forms['company-product']
			region:      $rootScope.forms['company-location']
			social:      $rootScope.forms['company-social']

		back: ->
			$scope.addCompany.go($scope.addCompany.currentStep-1)
			$TRACK({type:"back"})

		showBack: ->
			if $scope.addCompany.currentStep > 0 && $scope.addCompany.currentStep < $scope.addCompany.qSteps
				return true
			else
				return false


	$scope.addCompany.forms.sii.callback = ->
		$timeout ->
			$scope.addCompany.forms.sii.status = 'importing'
			$timeout ->
				$scope.addCompany.forms.sii.status = 'ready'
			, 5000
		, 20

	$scope.addCompany.forms.contactdata.callback = ->
		$timeout ->
			$scope.addCompany.go(2)
		, 1000

	$scope.addCompany.forms.product.callback = ->
		$timeout ->
			$scope.addCompany.go(3)
		, 1000

	#$scope.addCompany.forms.payment.callback = ->
	#	$timeout ->
	#		$scope.addCompany.go(4)
	#	, 1000

	$scope.addCompany.forms.region.callback = ->
		$timeout ->
			$scope.addCompany.go(4)
		, 1000

	#$scope.addCompany.forms.social.callback = ->
	#	$timeout ->
	#		$scope.addCompany.go(6)
	#	, 1000


	$scope.reset = ->
		$scope.addCompany.currentStep = 0
		$scope.addCompany.forms.sii.status = undefined

		for step in $scope.addCompany.steps
			$scope.addCompany.forms[step].success = false
			$scope.addCompany.forms[step].data = {}




	$scope.useMyData = (slug) ->
		#console.log slug
		#console.log $scope.addCompany.forms[slug]
		for part in $scope.addCompany.forms[slug].options
			#console.log part.name

			if part.name == 'name'
				$scope.addCompany.forms[slug].data[part.name] = 'Rodrigo Álvarez'
				#console.log $scope.addCompany.forms[slug].data[part.name] 

			if part.name == 'lastname'
				$scope.addCompany.forms[slug].data[part.name] = 'Álvarez'

			if part.name == 'rut'
				$scope.addCompany.forms[slug].data[part.name] = '16.659.192-9'

			if part.name == 'email'
				$scope.addCompany.forms[slug].data[part.name] = 'rodrigoalvarez@gmail.com'

			if part.name == 'phone'
				$scope.addCompany.forms[slug].data[part.name] = '64189776'




	$timeout ->
		$scope.reset()
		#$scope.addCompany.go(4)
		$scope.useMyData('contactdata')
	,100




	return


