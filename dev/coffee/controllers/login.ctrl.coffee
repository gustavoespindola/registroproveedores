
app.controller 'LoginCtrl', ($scope,$rootScope,$state,$timeout) ->

	if $rootScope.state.logged
		$state.go("pages.dashboard")

	$scope.login = $rootScope.forms.login

	$scope.login.callback = ->
		$timeout ->
			$state.go("pages.dashboard")
			$rootScope.state.logged = true
		, 1000


