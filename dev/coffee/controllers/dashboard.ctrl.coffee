
app.controller 'DashboardCtrl', ($scope,$API,$rootScope,$timeout) ->


	$scope.formFilters =
		data: {}



	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.dashboard'

	$scope.entries = [
		{title: "Abastecimiento y Transporte de Agua Potable"}
		{title: "Mantención Sistema Agua Potable"}
		{title: "Servicios de monitoreo y mantención de Agua Potable"}
		{title: "Abastecimiento y Transporte de Agua Potable"}
		{title: "Mantención Sistema Agua Potable"}
		{title: "Servicios de monitoreo y mantención de Agua Potable"}
	]

	$scope.showEntries = true

	$scope.changeEntries = ->
		$scope.showEntries = false
		$timeout ->
			$scope.showEntries = true
		,500

	$scope.filters =
		show: false
		open: ->
			this.show = true
		close: ->
			this.show = false



	# Custom select example

	#$scope.customSelectValue1 = 3

	$scope.customSelectOptions1 = [
		{id:1, name:"Opción 1"}
		{id:2, name:"Opción 2"}
		{id:3, name:"Opción 3"}
		{id:4, name:"Opción 4"}
		{id:5, name:"Opción 5"}
		{id:6, name:"Opción 6"}
		{id:7, name:"Opción 7"}
		{id:8, name:"Opción 8"}
	]

	$scope.customSelectOptions2 = [
		{id:1, name:"Opción 1"}
		{id:2, name:"Opción 2"}
		{id:3, name:"Opción 3"}
		{id:4, name:"Opción 4"}
	]

	$scope.customSelectOptions3 = [
		{id:1, name:"Opción A"}
		{id:2, name:"Opción B"}
	]

	# Custom scopebar

	$scope.customScopebarValue = 'relevant'

	$scope.customScopebarOptions = [
		{id:'relevant', name:"Más relevantes"}
		{id:'recents', name:"Más recientes"}
	]





