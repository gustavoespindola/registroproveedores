

app.controller 'SuppliersCtrl', ($scope,$state,$rootScope) ->

	$rootScope.currentPage = 'pages.suppliers'

	$scope.search = 
		string: ""
		go: () ->
			$state.go('pages.suppliers-search',{string: this.string })

	return


app.controller 'SuppliersSearchCtrl', ($scope,$state,$timeout,$rootScope,$API) ->

	$rootScope.currentPage = 'pages.suppliers'

	$scope.search = 
		string: $state.params.string
		go: () ->
			console.log "searching..."

	$scope.filters =
		show: false
		open: ->
			this.show = true
		close: ->
			this.show = false

	$scope.showEntries = true

	$scope.changeEntries = ->
		$scope.showEntries = false
		$timeout ->
			$scope.showEntries = true
		,500


	$API.getSuppliers({search:$state.params.string}).then (res) ->
		$scope.suppliers = res.data
		console.log "ooooo",res.data
		#console.log res.data




	# Custom select example

	#$scope.customSelectValue1 = 3

	$scope.customSelectOptions1 = [
		{id:1, name:"Opción 1"}
		{id:2, name:"Opción 2"}
		{id:3, name:"Opción 3"}
		{id:4, name:"Opción 4"}
		{id:5, name:"Opción 5"}
		{id:6, name:"Opción 6"}
		{id:7, name:"Opción 7"}
		{id:8, name:"Opción 8"}
	]

	$scope.customSelectOptions2 = [
		{id:1, name:"Opción 1"}
		{id:2, name:"Opción 2"}
		{id:3, name:"Opción 3"}
		{id:4, name:"Opción 4"}
	]

	$scope.customSelectOptions3 = [
		{id:1, name:"Opción A"}
		{id:2, name:"Opción B"}
	]


	$scope.customSelectOptions4 = [
		{id:1, name:"Opción 1"}
		{id:2, name:"Opción 2"}
		{id:3, name:"Opción 3"}
		{id:4, name:"Opción 4"}
	]

	$scope.customSelectOptions5 = [
		{id:1, name:"Opción A"}
		{id:2, name:"Opción B"}
	]

	# Custom scopebar

	$scope.customScopebarValue1 = 'all'

	$scope.customScopebarOptions1 = [
		{id:'all', name:"Todos"}
		{id:'name', name:"Nombre"}
		{id:'rut', name:"Rut"}
		{id:'product', name:"Producto"}
	]


	$scope.customScopebarValue2 = 'relevant'

	$scope.customScopebarOptions2 = [
		{id:'relevant', name:"Más relevantes"}
		{id:'recents', name:"Más recientes"}
	]




	return


app.controller 'SupplierCtrl', ($scope,$state,$rootScope,$stateParams,$API) ->

	$rootScope.currentPage = 'pages.suppliers'

	$scope.entryBack = ->
		window.history.back()

	#console.log $stateParams.id

	$API.getCompany({id:$stateParams.id}).then (res) ->
		$scope.supplier = res.data
		#console.log res.data




	return


app.controller 'SupplierDetailCtrl', ($scope,$state,$rootScope,$stateParams,$API) ->

	$rootScope.currentPage = 'pages.suppliers'

	$scope.entryBack = ->
		window.history.back()

	$scope.detailSlug = $state.params.detail

	$API.getCompany({id:$stateParams.id}).then (res) ->
		$scope.supplier = res.data
		#console.log res.data

	if $scope.detailSlug == 'products'
		$scope.tabValue = 1
	else
		$scope.tabValue = 2


	$scope.documents =  [
		{title:'Certificado de vigencia de sociedad'},
		{title:'Poder de representante legal'},
		{title:'Escritura de la empresa'}
	]





	return

