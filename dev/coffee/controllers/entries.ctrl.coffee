
app.controller 'EntriesCtrl', ($scope,$API,$rootScope) ->

	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.entries'

	$scope.entries = [
		{title: "Abastecimiento y Transporte de Agua Potable",liked:true}
		{title: "Mantención Sistema Agua Potable",liked:true}
		{title: "Servicios de monitoreo y mantención de Agua Potable",liked:true}
	]

	$scope.tab =
		current: 'offerted'
		switch: (name) ->
			this.current = name


app.controller 'EntryCtrl', ($scope,$API,$rootScope,$timeout) ->

	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.dashboard' if !$rootScope.currentPage

	$scope.entryBack = ->
		$scope.console.entry.push "entry back"
		window.history.back()

	$timeout ->
		$scope.entry = {}
	,1000



app.controller 'EntryApplyCtrl', ($scope,$API,$rootScope) ->

	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.dashboard' if !$rootScope.currentPage

	$scope.entryBack = ->
		window.history.back()

	$scope.form = $rootScope.forms['entry-apply']

