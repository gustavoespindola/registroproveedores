
app.controller 'ProductsCtrl', ($rootScope,$scope,$API) ->

	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.products'

	if $rootScope.isAvailableProducts
		$scope.isAvailableProducts = true


app.controller 'AddProductCtrl', ($rootScope,$scope,$API,$timeout,$state) ->

	$rootScope.state.logged = true
	$rootScope.currentPage = 'pages.products'

	$scope.addProduct = $rootScope.forms['product']

	$scope.addProduct.callback = ->
		$timeout ->
			$state.go("pages.products")
			$rootScope.isAvailableProducts = true
		, 3000		

	$timeout ->
		$scope.addProduct.success = false
		$scope.addProduct.data = {}
	,100

