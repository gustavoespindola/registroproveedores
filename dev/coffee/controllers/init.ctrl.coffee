
app.controller 'InitCtrl', ($scope,$rootScope,$timeout,$API,$MODAL,$USER,$q) ->

	#console.log "InitCtrl"

	$scope.console = {}

	$rootScope.config = {}
	$rootScope.state =
		loading: true
		logged:  false # true # false


	$scope.usermenu =
		show: false
		open: ->
			this.show = true			
		close: ->
			this.show = false
	
	###
	$scope.navdirect =
		collapsed: true
		toggle: ->
			if this.collapsed
				this.collapsed = false
			else
				this.collapsed = true
	###

	promises = []


	# MODAL -------------------------
	$MODAL.init()
	$scope.modal = $rootScope.modal


	# FORMS -------------------------
	promises.push $API.getForms()


	# USER --------------------------
	$scope.user = $USER
	promises.push $API.getMe()


	# PROMISES ----------------------

	$q.all(promises).then (res) ->

		if res[0].data && res[1].data

			$rootScope.state.loading = false

			# FORMS
			if res[0].data
				$rootScope.forms = res[0].data

			# USER
			if res[1].data
				$USER.setData(res[1].data)


	# COMPANY -----------------------

	$scope.changeCompany = (id) ->
		$USER.setCompany(id)
		$rootScope.state.loading = true
		$timeout ->
			$scope.usermenu.close()
			$timeout ->
				$rootScope.state.loading = false
			,900
		,300




app.controller 'PagesCtrl', () ->
	#console.log "PagesCtrl"


