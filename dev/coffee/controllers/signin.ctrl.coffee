
app.controller 'SigninCtrl', ($scope,$rootScope,$timeout,$TRACK,$state) ->

	$rootScope.currentPage = 'pages.signin'

	if $rootScope.state.logged
		$state.go("pages.dashboard")


	processData =
		rut: 
			[
				{
					title: "Usuario"
					steps: [
						{
							title: "Ingresa los datos de tu cédula de identidad"
							description: "Ingresa el rut, número de serie o número de documento."
							form: $rootScope.forms["signin-rut"]
						},{
							title: "Ingresa los datos de contacto y contraseña"
							description: "Estos datos nos permitirán mantenerte informado de novedades y oportunidades de negocio en ChileCompra."
							form: $rootScope.forms["signin-contact"]
						},{
							title: "Hemos enviado un código de verificación a tu cuenta de correo"
							description: "Ingresa el código de verificación que hemos enviado a tu email"
							form: $rootScope.forms["signin-validation"]
						},{
							title: "Te damos la bienvenida!"
							description: "Ya puedes utilzar la plataforma con los datos ingresados. Ahora solo debes completar los datos de tu empresa."
							button: "Asociar mi empresa"
						}
					]
				},{
					title: "Empresa"
					steps: [
						{
							title: "Ingresa los datos de tu empresa"
							description: "Podrás modificar posteriormente estos datos desde tu escritorio empresa."
							form:  $rootScope.forms["company-info"]
						},{
							title: "Ingresa a dirección de la casa matriz de tu empresa"
							form:  $rootScope.forms["company-address"]
						},{
							title: "Háblanos un poco sobre tu empresa"
							form:  $rootScope.forms["company-features"]
						},{
							title: "Selecciona el lugar en el que deseas venderle al estado"
							form:  $rootScope.forms["company-location"]
						},{
							title: "Perfecto! ya puedes comenzar a operar."
							description: "Ya tienes tu inscripción básica. Con ella podrás:"
							list: ["Conocer oportunidades de negocios con el Estado","Presentar ofertas y cotizaciones","Participar en licitaciones", "Actualizar y completar tus datos desde la sección Mi Perfil"]
							description2: "Importante: Puede que necesites una inscripción avanzada (pagada) para:"
							list2: ["Demostrar por sistema que no tienes inhabilidades para contratar con el Estado.", "Poder firmar un contrato que te adjudiques."]
							button: "Ir a mi escritorio"
						}
					]
				}
			]

		foreign: 
			[
				{
					title: "Usuario"
					steps: [
						{
							title: "Ingresa tus datos de contacto y una contraseña"
							form: $rootScope.forms["signin-f-basic"]
						},
						{
							title: "Hemos enviado a tu email un código de verificación"
							description: "Ingresa el código que hemos enviado a tu email."
							form: $rootScope.forms["signin-validation"]
						},
						{
							title: "¡Listo! ya estás registrado como usuario en ChileCompra"
							description: "Ya puedes utilizar la plataforma con los datos ingresados. Ahora sólo debes completar los datos de tu empresa."
							button: "Completar datos de mi empresa"
						}
					]
				},{
					title: "Empresa"
					steps: [
						{
							title: "Completa los datos de tu empresa"
							form: $rootScope.forms["signin-f-company"]
						},
						{
							title: "¡Listo! tu empresa ya puede operar dentro de ChileCompra"
							description: "Ya puedes utilizar la plataforma con los datos ingresados."
							button: "Ir a mi escritorio"
						}
					]
				}
			]


		claveunica:
			[
				{
					title: "Usuario"
					steps: [
						{
							title: "Ingresa los datos de contacto y contraseña"
							description: "Estos datos nos permitirán mantenerte informado de novedades y oportunidades de negocio en ChileCompra."
							form: $rootScope.forms["signin-contact-b"]
						},{
							title: "Hemos enviado un código de verificación a tu cuenta de correo"
							description: "Ingresa el código de verificación que hemos enviado a tu email"
							form: $rootScope.forms["signin-validation"]
						},{
							title: "Te damos la bienvenida!"
							description: "Ya puedes utilzar la plataforma con los datos ingresados. Ahora solo debes completar los datos de tu empresa."
							button: "Asociar mi empresa"
						}
					]
				},{
					title: "Empresa"
					steps: [
						{
							title: "Ingresa los datos de tu empresa"
							description: "Podrás modificar posteriormente estos datos desde tu escritorio empresa."
							form:  $rootScope.forms["company-info"]
						},{
							title: "Ingresa a dirección de la casa matriz de tu empresa"
							form:  $rootScope.forms["company-address"]
						},{
							title: "Háblanos un poco sobre tu empresa"
							form:  $rootScope.forms["company-features"]
						},{
							title: "Selecciona el lugar en el que deseas venderle al estado"
							form:  $rootScope.forms["company-location"]
						},{
							title: "Perfecto! Tu ya puedes comenzar a operar."
							description: "Ya tienes tu inscripción básica. Con ella podrás:"
							list: ["Conocer oportunidades de negocios con el Estado","Presentar ofertas y cotizaciones","Participar en licitaciones", "ctualizar y completar tus datos desde la sección Mi Perfil"]
							description2: "Importante: Puede que necesites una inscripción avanzada (pagada) para:"
							list2: ["Demostrar por sistema que no tienes inhabilidades para contratar con el Estado.", "Poder firmar un contrato que te adjudiques."]
							button: "Ir a mi escritorio"
						}
					]
				}
			]




	$scope.signin =
		data: {}
		process: undefined
		current:
			group: 0
			step: 0

		start: (name) ->

			this.data = processData[name]
			this.process = name

			for step in this.data
				for stp in step.steps
					if stp.form && !stp.form.callback
						stp.form.callback = ->
							$scope.signin.next()

		next: ->

			if this.data[this.current.group].steps[this.current.step+1]
				this.current.step++
			else
				if this.current.step==this.data[this.current.group].steps.length-1

					if this.data[this.current.group+1]
						this.current.step=0
						this.current.group++
					else
						$state.go("pages.dashboard")

			$rootScope.state.stepChanging = true
			$timeout ->
				$rootScope.state.stepChanging = false
			,500

		reset: ->
			this.current.group = 0
			this.current.step  = 0




		#back: ->
		#	$TRACK({type:"back"})
		#	$scope.signin.go($scope.signin.currentStep-1)






	return

