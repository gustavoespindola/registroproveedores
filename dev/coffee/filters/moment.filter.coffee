
app.filter 'moment', ->
	(input, momentFn) ->
		args = Array.prototype.slice.call(arguments, 2)
		momentObj = moment(input)
		return momentObj[momentFn].apply(momentObj, args)

