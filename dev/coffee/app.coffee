

app = angular.module 'app', [
	'ui.router'
	'ct.ui.router.extras'
	'ngAnimate'
	'ngSanitize'
	#'ngTouch'
	#'angular-fastclick'
	#'swipe'
	'ngTagsInput'
	'mightyDatepicker'
]

#=include config/*
#=include controllers/*
#=include directives/*
#=include services/*
#=include filters/*

