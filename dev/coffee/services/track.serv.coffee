
app.service "$TRACK", ($state,$location) ->

	track = (params) ->

		#console.log "tracking", $state.current.name, $location.path()
		#console.log params

		if params.type=='pageview'

			gtag('event', 'pageview', {
			  'event_category': 'pageviews',
			  'event_label': $state.current.name,
			  'value': $location.path()
			})

			hj('trigger', 'pageview', $state.current.name)

		if params.type=='step'

			gtag('event', 'pageview', {
			  'event_category': 'steps',
			  'event_label': params.type,
			  'value': params.value
			})

			hj('trigger', 'pageview', 'step', params.type, params.value)

		if params.type=='back'

			gtag('event', 'back', {
			  'event_category': 'actions',
			  'event_label': 'back'
			})

			hj('trigger', 'back')


	return track
