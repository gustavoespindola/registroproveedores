
app.service "$USER", ($rootScope) ->

	if !$rootScope.user
		$rootScope.user = {}

	service =
		data: $rootScope.user
		currentCompanyId: undefined
		isLogged: false

		setData: (data) ->

			for k,v of data
				this.data[k] = v

			this.currentCompanyId = data.companies[0].id

			this.isLogged = true

		setCompany: (id) ->
			this.currentCompanyId = id


	return service
