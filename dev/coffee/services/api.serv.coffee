
app.service "$API", ($http,$rootScope) ->


	# ------------ ENDPOINTS -----------

	# Forms

	getForms: (data) ->
		return this.dev.get "json/forms.json"


	# General

	postFake: (data) ->
		return this.dev.post "json/post.json", data

	getMe: () ->
		return this.dev.get "json/me.json"

	getCompany: (params) ->
		return this.dev.get "json/company-"+params.id+".json", params

	getSuppliers: (params) ->
		return this.dev.get "json/suppliers.json", params


	# Integration

	postValidarCedula: (data) ->
		return this.post URL_API+"/usuarios/validar-cedula", data

	postRegistrarUsuario: (data) ->
		return this.post URL_API+"/usuarios", data

	postValidarUsuario: (data) ->
		return this.post URL_API+"/correos/validar", data


	###
	postLogin: (data) ->
		return this.post URL_API + '/login', data

	postSignin: (data) ->
		return this.post URL_API + '/signin', data

	getConfig: () ->
		return this.get URL_API + '/config'
	###


	# ------------- METHODS -------------

	dev:

		headers: () ->
			headers = {}
			#headers['Content-Type'] = undefined
			#headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
			headers['Authorization'] = "Bearer "+$rootScope.token if $rootScope.token
			return headers

		get: (url,params) ->
			params = {} if !params
			#params.Authorization = $rootScope.token if $rootScope.token
			return $http
				url: url
				method: 'GET'
				headers: this.headers()
				params: params

		post: (url,data) ->
			return $http
				url: url
				method: 'POST'
				headers: this.headers()
				data: data



	headers: () ->
		headers = {}
		headers['ocp-apim-subscription-key'] = "f317dd7b7f5d40ceb2479b4cf73ea385"
		headers['Content-Type'] = "application/json"
		return headers

	get: (url,params) ->
		params = {} if !params
		#params.Authorization = $rootScope.token if $rootScope.token
		return $http
			url: url
			method: 'GET'
			headers: this.headers()
			params: params

	post: (url,data) ->
		return $http
			url: url
			method: 'POST'
			headers: this.headers()
			data: data



