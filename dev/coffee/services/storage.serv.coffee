
app.service "$STORAGE", ($rootScope) ->

	saveToken: (token) ->
		window.localStorage.setItem("token", token)

	readToken: () ->
		window.localStorage.getItem("token")

	clear: ->
		localStorage.removeItem("token")

