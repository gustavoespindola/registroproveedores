
app.service "$MODAL", ($rootScope) ->

	service =

		init: ->
			$rootScope.modal =
				open: (html) ->					
					service.open(html)
				close: ->
					service.close()
				show: false
				html: ""

		open: (html) ->
			$rootScope.modal.show = true 
			$rootScope.modal.html = html

		close: ->
			$rootScope.modal.show = false
	

	return service
