
app.directive 'formElement', ($timeout,$rootScope,$MODAL) ->
	restrict: 'E'
	template: '<div class="form__element form__element--{{element.type}} form__element--{{element.variant}}"><div class="form__element__title">{{element.label}}<span ng-if="element.required" class="form__element__required"> *</span></div><div class="form__element__input"><input type="text" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="element.type==\'text\'" ng-disabled="element.disabled" autocomplete="off" placeholder="{{element.placeholder}}"/><input type="email" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="element.type==\'email\'" ng-disabled="element.disabled" autocomplete="off" placeholder="{{element.placeholder}}"/><label ng-if="element.type==\'password\'" class="form__element__password"><div ng-if="!showPassword.show"><input type="password" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-disabled="element.disabled" autocomplete="off"/></div><div ng-if="showPassword.show"><input type="text" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-disabled="element.disabled" autocomplete="off"/></div><div ng-click="showPassword.toggle()" class="form__element__eye"><span ng-if="!showPassword.show" class="fa fa-eye"></span><span ng-if="showPassword.show" class="fa fa-eye-slash"></span></div></label><input type="number" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if=" element.type==\'number\'" ng-disabled="element.disabled" min="{{element.min}}" max="{{element.max}}" maxlength="{{element.maxlength}}" step="{{element.step}}" placeholder="{{element.placeholder}}"/><div ng-if="element.type==\'phone\'" class="form__element__prefix">+569</div><input type="number" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="element.type==\'phone\'" ng-disabled="element.disabled" maxlength="8" max="99999999" placeholder="{{element.placeholder}}"/><input type="text" ng-model="form.data[element.name]" ng-change="form.validate(element);formatRut()" ng-if="element.type==\'rut\'" ng-disabled="element.disabled" autocomplete="off" placeholder="{{element.placeholder}}" maxlength="12"/><div ng-if="element.type==\'tags\'" class="form__element form__element--tags"><tags-input ng-model="form.data[element.name]" max-tags="3"></tags-input></div><div ng-if="element.type==\'file\' &amp;&amp; !form.data[element.name]" class="form__element__file"><input type="file" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="form.getOS()==\'unknown\'" ng-disabled="element.disabled" accept="image/*" capture="capture" file="file"/><input type="file" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="form.getOS()==\'android\'" ng-disabled="element.disabled" capture="capture" file="file"/><input type="file" ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="form.getOS()==\'ios\'" ng-disabled="element.disabled" accept="image/*" file="file"/><div class="form__element__file__input"><div class="btn btn--border btn--block"><span class="fa fa-camera"></span> Seleccionar</div></div></div><div ng-if="element.type==\'file\' &amp;&amp; form.data[element.name]" class="form__file"><div class="form__file__preview form__file__preview--{{form.data[element.name].orientation}}"><img ng-src="{{form.data[element.name].base64}}" ng-if="form.data[element.name].base64"/><img ng-src="https://res.cloudinary.com/xxxxxxx/image/upload/w_200,h_200,c_fill,g_face/{{form.data[element.name].url}}" ng-if="form.data[element.name].url"/></div><div ng-click="removeFile()" class="form__file__remove"><span class="fa fa-trash"></span></div></div><select ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="element.type==\'select\' || element.type==\'feeder\'" ng-disabled="element.disabled"><option ng-repeat="opt in element.options" ng-value="opt.value">{{opt.text}}</option></select><textarea ng-model="form.data[element.name]" ng-change="form.validate(element)" ng-if="element.type==\'textarea\'" ng-disabled="element.disabled" maxlength="{{element.maxlength}}" placeholder="{{element.placeholder}}"></textarea><div ng-if="element.type==\'textarea\' &amp;&amp; element.maxlength" class="form__element__counter">{{element.maxlength - form.data[element.name].length}}</div><label ng-repeat="opt in element.options track by $index" ng-if="element.type==\'radio\'" ng-class="{\'label--disabled\': element.disabled}" class="form__element__label"><input type="radio" ng-model="form.data[element.name]" ng-value="opt.value" ng-disabled="element.disabled"/> {{opt.text}}</label><label ng-repeat="opt in element.options track by $index" ng-if="element.type==\'checkbox\'" ng-class="{\'label--disabled\': element.disabled}" class="form__element__label"><input type="checkbox" ng-model="form.data[element.name][$index]" ng-value="opt.value" ng-true-value="{{opt.value}}" ng-false-value="null" ng-disabled="element.disabled"/> {{opt.text}}</label><label ng-if="element.type==\'terms\'" class="form__element__label"><input type="checkbox" ng-model="form.data[element.name]" ng-value="opt.value" ng-true-value="1" ng-false-value="0"/> Acepto los <a ng-click="openModal(element.modalB)">términos y condiciones</a></label><div ng-if="element.type==\'code\'" ng-init="form.data[element.name]=[]" class="form__element__code"><input type="number" ng-model="form.data[element.name][0]" maxlength="1" max="9" movefocus="movefocus"/><input type="number" ng-model="form.data[element.name][1]" maxlength="1" max="9" movefocus="movefocus"/><input type="number" ng-model="form.data[element.name][2]" maxlength="1" max="9" movefocus="movefocus"/><input type="number" ng-model="form.data[element.name][3]" maxlength="1" max="9" movefocus="movefocus"/></div><div ng-if="element.type==\'date\'" ng-class="{\'form__element__datepicker--opened\': datepicker.show, \'form__element__datepicker--range\': element.range}" class="form__element__datepicker"><div ng-click="datepicker.close()" ng-if="datepicker.show" class="form__element__datepicker__bg"></div><div ng-if="element.range &amp;&amp; element.before" class="form__element__prefix">Desde</div><div ng-if="element.range &amp;&amp; element.after" class="form__element__prefix">Hasta</div><div ng-click="datepicker.toggle()" class="form__element__datepicker__input">{{form.data[element.name] | moment: \'format\': \'DD/MM/YYYY\'}}</div><div ng-if="datepicker.show" class="form__element__datepicker__mighty"><mighty-datepicker ng-model="form.data[element.name]" options="element.options" ng-if="!element.range"></mighty-datepicker><mighty-datepicker ng-model="form.data[element.name]" options="element.options" range-from="true" before="form.data[element.before]" ng-if="element.range &amp;&amp; element.before"></mighty-datepicker><mighty-datepicker ng-model="form.data[element.name]" options="element.options" range-to="true" after="form.data[element.after]" ng-if="element.range &amp;&amp; element.after"></mighty-datepicker></div></div></div><div class="form__element__description">{{element.description}}</div><div ng-if="element.modal" ng-click="openModal(element.modal)" class="form__element__explain"><span class="fa fa-info-circle"></span> {{element.explain}}</div><div ng-if="element.valid==false" class="form__element__validation">{{element.message}}</div></div>'
	#templateUrl: 'directives/form-element.directive.html'
	replace: true
	scope:
		form: "="
		element: "="
	link: ($scope,el,attrs) ->

		$scope.ENV = "web"
		$scope.ENV = "cordova" if navigator.camera


		if typeof $scope.element.options == "string"
			if $scope.element.options.indexOf "$" == 0
				string = $scope.element.options.replace "$", ""
				#console.log $rootScope.config[string]
				if $rootScope.config[string]
					$scope.element.options = $rootScope.config[string]

		if !$scope.form.data[$scope.element.name] && $scope.element.value
			$scope.form.data[$scope.element.name] = $scope.element.value



		#console.log $scope.element

		if $scope.element.type == 'file'

			$scope.removeFile = ->
				$scope.form.data[$scope.element.name] = undefined

			$scope.webcam =
				capture: ->
					navigator.camera.getPicture (data) ->
						$scope.form.data[$scope.element.name] =
							base64: data
							from: "app-camera"
						$scope.$apply()
					, (data) ->
						console.log("Fail")
					, {quality: 20, destinationType: Camera.DestinationType.DATA_URL}


		# Format rut

		$scope.formatRut = ->
	
			#console.log $scope.form.data[$scope.element.name]

			if $scope.form.data[$scope.element.name]

				newvalue = ""
				splitRut = $scope.form.data[$scope.element.name].replace(/[^0-9kK]+/g, "").split("")

				if splitRut.length > 0
					for i in [(splitRut.length-1)..0]
						#if splitRut.length > 7
						if i == splitRut.length-1-3 - 1
							newvalue = "." + newvalue
						if i == splitRut.length-1-6 - 1
							newvalue = "." + newvalue
						newvalue = splitRut[i] + newvalue
						if i == splitRut.length-1 && splitRut.length > 4
							newvalue = "-" + newvalue

					#console.log "nv", newvalue
					#console.log $scope.form.data
					$scope.form.data[$scope.element.name] = newvalue
					#console.log "ddd", $scope.form.data[$scope.element.name]


		$scope.element.focus = undefined
		$scope.focus = (name) ->
			$scope.element.focus = name
			#console.log name,$scope.element.focus


		$scope.showPassword = 
			show: false
			toggle: ->
				if !this.show
					this.show = true
				else
					this.show = false


		$scope.openModal = (html) ->
			$MODAL.open(html)


		$scope.datepicker = 
			show: false
			toggle: ->
				if !this.show
					this.show = true
				else
					this.show = false
			open: ->
				this.show = true
			close: ->
				this.show = false


		if $scope.element.type=='date'
			console.log "options"
			$scope.element.options =
				callback: (e) ->
					$scope.form.data[$scope.element.name] = e._d
					$scope.datepicker.close()
					return




