
app.directive 'movefocus', () ->
	restrict: 'A'
	link: ($scope,el) ->

		el.on "input", (e) ->

			if el.val().length == parseInt(el.attr("maxlength"))
				$nextEl = el.next()

				if $nextEl.length
					$nextEl[0].focus()

