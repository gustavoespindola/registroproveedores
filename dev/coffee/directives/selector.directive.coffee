
app.directive 'selector', () ->
	restrict: 'E'
	templateUrl: 'directives/selector.directive.html'
	replace: true
	scope:
		options: "="
		value: "="
		placeholder: "@"

	link: ($scope,el) ->

		#console.log $scope.options, $scope.value

		$scope.selector =
			oppened: false
			toggle: ->
				if this.oppened
					this.oppened = false
				else
					this.oppened = true

			select: (opt) ->
				this.oppened = false
				$scope.value = opt.id

			getName: (id) ->
				name = $scope.placeholder
				for opt in $scope.options
					if opt.id == id
						name = opt.name
				return name



