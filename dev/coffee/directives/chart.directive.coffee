
app.directive 'chart', () ->
	restrict: 'E'
	templateUrl: 'directives/chart.directive.html'
	replace: true
	scope:
		values: "="
		labels: "="
	link: ($scope,el) ->

		console.log "values", $scope.values

		# Get max
		$scope.max = 0
		for val in $scope.values
			$scope.max = val if val > $scope.max
		
		$scope.maxCeiled = Math.ceil($scope.max/1000000) * 1000000


		$scope.axis = []
		axisPartitions = 4
		for i in [0..axisPartitions]
			$scope.axis.push $scope.maxCeiled / axisPartitions * i

		$scope.axis = $scope.axis.reverse()

		console.log $scope.axis

