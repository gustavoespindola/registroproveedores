
app.directive 'scopebar', () ->
	restrict: 'E'
	templateUrl: 'directives/scopebar.directive.html'
	replace: true
	scope:
		options: "="
		value: "="
	link: ($scope,el) ->

		$scope.scopebar =

			select: (opt) ->
				this.oppened = false
				$scope.value = opt.id

			getName: (id) ->
				name = false
				for opt in $scope.options
					if opt.id == id
						name = opt.name
				return name


