
app.directive 'donnut', ($timeout) ->
	restrict: 'E'
	template: "<div class='donnut'><canvas></canvas><div class='donnut__value'>{{percent}}%</dov></div>"
	replace: true
	scope:
		value: "="
		total: "="
	link: ($scope,el) ->

		$scope.percent = $scope.value * 100 / $scope.total

		console.log el

		canvas = el[0].getElementsByTagName("canvas")[0]

		size = 400
		canvas.width = size
		canvas.height = size

		totalPi = 2 * Math.PI
		percentInPi = $scope.percent * totalPi / 100

		startPoint = Math.PI * -0.5
		endPoint   = percentInPi - (Math.PI*0.5)

		lineWidth = 20

		ctx = canvas.getContext("2d")


		#easeInOutQuad = (t) ->
		#	t<.5 ? 2*t*t : -1+(4-2*t)*t

		draw = (to) ->

			ctx.beginPath()
			ctx.arc(size/2, size/2, size/2-lineWidth, startPoint, totalPi)
			ctx.strokeStyle = "#e7ecff";
			ctx.lineWidth = lineWidth
			ctx.stroke()

			cEnd = (to - startPoint) * 1 / (totalPi - startPoint)

			ctx.beginPath()
			ctx.arc(size/2, size/2, size/2-lineWidth, startPoint, to)
			ctx.strokeStyle = "#2E5BFF";
			ctx.lineWidth = lineWidth
			ctx.stroke()


		to = startPoint

		draw(to)

		drawNext = ->

			$timeout ->

				ctx.clearRect(0, 0, size, size)
				draw(to)

				if to < endPoint
					draw(to)
					drawNext()
					to = to + 0.03
				else
					draw(endPoint)

			,5

		$timeout ->
			drawNext()
		,500








