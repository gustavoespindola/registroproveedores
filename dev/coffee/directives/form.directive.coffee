
app.directive 'formAuto', ($API,$rootScope,$timeout) ->
	restrict: 'E'
	template: '<form ng-submit="form.submit()" class="form"><form-element ng-repeat="element in form.options" ng-if="showElement(element)" element="element" form="form"></form-element><div ng-if="form.message &amp;&amp; !form.success" class="form__message">{{form.message}}</div><div class="form__element"><div class="form__submit"><button ng-disabled="form.loading || form.success" class="btn btn--md btn--block">{{form.submitname}} <span class="fal fa-angle-right"></span></button></div><div ng-if="form.loading" class="form__loader"><loader></loader></div><div ng-if="form.success" class="form__success"><span class="fa fa-check"></span></div></div></form>'
	#templateUrl: 'directives/form.directive.html'
	replace: true
	scope:
		form: "=?"
		formJson: "@?"
	link: ($scope,el,attrs) ->


		$scope.form            = {} if !$scope.form
		#console.log $rootScope.forms[$scope.formJson]
		#$scope.form            = $rootScope.forms[$scope.formJson] if $rootScope.forms[$scope.formJson]

		$scope.form.loading    = false
		$scope.form.message    = undefined
		$scope.form.success    = false
		$scope.form.submitname = "Guardar" if !$scope.form.submitname
		$scope.form.data       = {} if !$scope.form.data



		# Loop: Format numbers / others

		for opt in $scope.form.options

			if opt.type=='number' || opt.type=='unit'
				if $scope.form.data[opt.name]
					$scope.form.data[opt.name] = parseFloat($scope.form.data[opt.name])



		$scope.form.validateAll = () ->

			pass = true

			for element in $scope.form.options
				if !$scope.form.validate(element)
					pass = false

			return pass

		$scope.form.validate = (element) ->

			element.valid = true

			if element.name

				val = $scope.form.data[element.name]

				if element.required

					if !element.dependsOf
						if !(val || val==false || val==0)
							element.valid = false
							element.message = "Este campo es obligatorio"

					else
						tocompare = element.dependsOf.split("==")
						tocompareEl = $scope.form.data[tocompare[0]]

						if tocompareEl == tocompare[1]

							if !(val || val==false || val==0)
								element.valid = false
								element.message = "Este campo es obligatorio"



				#if element.type == "rut"
				#	if !validateRut( $scope.form.data[element.name] )
				#		element.valid = false

			return element.valid



		if !$scope.form.submit

			$scope.form.submit = ->

				$scope.form.message = undefined
				$scope.form.messageDetails = undefined

				if $scope.form.validateAll()

					$scope.form.loading = true

					$API[$scope.form.api]($scope.form.data).then (res) ->

						$timeout ->

							$scope.form.loading = false

							if res.data.success
								$scope.form.message = "Success"
								$scope.form.success = true
								$scope.form.callback(res) if $scope.form.callback

							else
								$scope.form.message = "Error (success) " + $scope.form.api

							$scope.form.message        = res.data.message if res.data && res.data.message
							$scope.form.message        = res.data.data.message if res.data && res.data.data && res.data.data.message
							#$scope.form.messageDetails = res.data.details if res.data && res.data.details

						,500

					, (res) ->

						### ONLY DEV
						$scope.form.loading = false
						$scope.form.message = "Error (fail) " + $scope.form.api
						$scope.form.message = res.data.message if res.data && res.data.message
						$scope.form.messageDetails = res.data.details if res.data && res.data.details
						$scope.form.fallback(res) if $scope.form.fallback
						###
						$timeout ->
							$scope.form.loading = false
							$scope.form.message = "Success"
							$scope.form.success = true
							$scope.form.callback(res) if $scope.form.callback
						,500						

				else
					$scope.form.message = "Faltan campos que completar"



		$scope.form.getOS = ->

			userAgent = navigator.userAgent || navigator.vendor || window.opera

			if (/android/i.test(userAgent))
				return "android";

			if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)
				return "ios"

			return "unknown"



		validateRut = (string) ->
			T = string.replace(/[^0-9kK]+/g, "").slice(0,-1)
			P = parseInt(T)
			M = 0
			S = 1
			while T
				S = (S + T % 10 * (9 - (M++ % 6))) % 11
				T = Math.floor(T / 10)
			if S
				V = S - 1
			else
				V = 'K'

			#console.log T, P, M, S, V
			if (V=="K" && string.slice(-1).toUpperCase()==V) || (V!="K" && parseInt(string.slice(-1))==V)
				#if !company
				return true
				#else
				#	if P ==x
			else
				return false





		$scope.showElement = (element) ->
			if element.dependsOf
				vals = element.dependsOf.split("==")
				if $scope.form.data[vals[0]]==vals[1]
					return true
				else
					return false
			else
				return true




