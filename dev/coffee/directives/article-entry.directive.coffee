
app.directive 'articleEntry', ($API) ->
	restrict: 'E'
	templateUrl: 'directives/article-entry.directive.html'
	replace: true
	scope:
		data: "="
	link: ($scope,el) ->

		$scope.like = ->
			if !$scope.data.liked
				$scope.data.liked = true
			else
				$scope.data.liked = false


