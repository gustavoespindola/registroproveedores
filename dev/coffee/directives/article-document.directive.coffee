
app.directive 'articleDocument', ($API) ->
	restrict: 'E'
	templateUrl: 'directives/article-document.directive.html'
	replace: true
	scope:
		data: "="
