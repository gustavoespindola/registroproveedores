
app.directive 'loader', ($timeout) ->
	restrict: 'E'
	templateUrl: 'directives/loader.directive.html'
	replace: true
	scope:
		variant: "="

