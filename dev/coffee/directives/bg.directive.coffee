
app.directive 'bg', ($interval,$rootScope) ->
	restrict: 'E'
	template: '<div class="bg" ng-style="bg.styles" ng-class="{\'bg--loaded\': bg.loaded}"></div>'
	replace: true
	scope:
		image: "@"
	link: ($scope,el) ->
		#console.log "bg-----"
		#console.log $scope.src

		$scope.bg =
			loaded: false
			styles: {}

		$scope.bg.styles =
			backgroundImage: 'url('+$scope.image+')'

		img = new Image()
		img.src = $scope.image
		#console.log "LOADINGG"
		#console.log img
		img.onload = ->
			#console.log "LOADEDDD"
			#console.log $scope.src
			$scope.bg.loaded = true
			$scope.$apply()
