
moment.updateLocale 'es',
	months:			'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
	monthsShort:	'Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dec'.split('_'),
	weekdays:		'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
	weekdaysShort:	'Dom_Lun_Mar_Mier_Jue_Vier_Sab'.split('_'),
	weekdaysMin:	'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
	relativeTime:
		future:		'en %s',
		past:		'%s', # ago
		s:			'1 segundo',
		ss:			'%d segs',
		m:			'1 min',
		mm:			'%d mins',
		h:			'1 hora',
		hh:			'%d hrs',
		d:			'1 día',
		dd:			'%d días',
		M:			'1 mes',
		MM:			'%d meses',
		y:			'1 año',
		yy:			'%d años'

moment.updateLocale 'en',
	relativeTime:
		past:		'%s', # ago


