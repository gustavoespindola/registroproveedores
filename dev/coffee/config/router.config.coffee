
app.config ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) ->

	# HTML5 mode only for web app. Cordova app runs on file://

	#if window.location.href.indexOf("file://") < 0
	#$locationProvider.html5Mode(true)

	# Routes

	$urlRouterProvider.otherwise('/')

	$stateProvider

		.state 'pages',
			views:
				'pages':
					controller: "PagesCtrl"
			#sticky: true
			#dsr: true

		.state 'pages.home',
			url: '/'
			templateUrl: 'views/home.view.html'
			controller: "HomeCtrl"

		.state 'pages.login',
			url: '/login'
			templateUrl: 'views/login.view.html'
			controller: "LoginCtrl"

		.state 'pages.signin',
			url: '/signin'
			templateUrl: 'views/signin.view.html'
			controller: "SigninCtrl"

		.state 'pages.add-company',
			url: '/add-company'
			templateUrl: 'views/add-company.view.html'
			controller: "AddCompanyCtrl"

		.state 'pages.dashboard',
			url: '/dashboard'
			templateUrl: 'views/dashboard.view.html'
			controller: "DashboardCtrl"
			sticky: true

		.state 'pages.entries',
			url: '/entries'
			templateUrl: 'views/entries.view.html'
			controller: "EntriesCtrl"

		.state 'pages.entry',
			url: '/entry/{id}'
			templateUrl: 'views/entry.view.html'
			controller: "EntryCtrl"
			viewVariant: "fullscreen"

		.state 'pages.entry-apply',
			url: '/entry/{id}/apply'
			templateUrl: 'views/entry-apply.view.html'
			controller: "EntryApplyCtrl"
			viewVariant: "fullscreen"

		.state 'pages.companies',
			url: '/companies'
			templateUrl: 'views/companies.view.html'
			controller: "CompaniesCtrl"

		.state 'pages.company-certification',
			url: '/company/{id}/certification'
			templateUrl: 'views/company-certification.view.html'
			controller: "CompanyCertificationCtrl"

		.state 'pages.company-documents',
			url: '/company/{id}/documents'
			templateUrl: 'views/company-documents.view.html'
			controller: "CompanyDocumentsCtrl"


		.state 'pages.products',
			url: '/products'
			templateUrl: 'views/products.view.html'
			controller: "ProductsCtrl"

		.state 'pages.add-product',
			url: '/products/add'
			templateUrl: 'views/add-product.view.html'
			controller: "AddProductCtrl"

		.state 'pages.form',
			url: '/account/{form}'
			templateUrl: 'views/form.view.html'
			controller: "FormCtrl"





		.state 'pages.suppliers',
			url: '/suppliers'
			templateUrl: 'views/suppliers.view.html'
			controller: "SuppliersCtrl"

		# Delete this urls... -----
		.state 'pages.suppliers2',
			url: '/suppliers/'
			templateUrl: 'views/suppliers.view.html'
			controller: "SuppliersCtrl"

		.state 'pages.suppliers3',
			url: '/supplier'
			templateUrl: 'views/suppliers.view.html'
			controller: "SuppliersCtrl"

		.state 'pages.suppliers4',
			url: '/supplier/'
			templateUrl: 'views/suppliers.view.html'
			controller: "SuppliersCtrl"
		# ------------------------

		.state 'pages.suppliers-search',
			url: '/suppliers/search/{string}'
			templateUrl: 'views/suppliers-search.view.html'
			controller: "SuppliersSearchCtrl"

		.state 'pages.supplier',
			url: '/supplier/{id}'
			templateUrl: 'views/supplier.view.html'
			controller: "SupplierCtrl"
			viewVariant: "fullscreen"

		.state 'pages.supplier-detail',
			url: '/supplier/{id}/{detail}'
			templateUrl: 'views/supplier-detail.view.html'
			controller: "SupplierDetailCtrl"
			viewVariant: "fullscreen"




		.state 'pages.notifications',
			url: '/notifications'
			templateUrl: 'views/notifications.view.html'
			controller: "NotificationsCtrl"

		.state 'pages.notifications-config',
			url: '/notifications/config'
			templateUrl: 'views/notifications-config.view.html'
			controller: "NotificationsConfigCtrl"










