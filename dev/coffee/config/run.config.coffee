

app.run ($rootScope, $state, $timeout, $TRACK, $locale) ->

	sendEvents = ->
		$rootScope.state.viewVariant = $state.current.viewVariant
		$TRACK({type:"pageview"})


	$rootScope.$on '$locationChangeStart', ->
		$rootScope.state.historyChanging = true
		#console.log "changing...."

	$rootScope.$on '$locationChangeSuccess', ->
		$timeout ->
			sendEvents()
			$rootScope.state.historyChanging = false
			#console.log "changed!"
		,20


	$locale.NUMBER_FORMATS.GROUP_SEP = "."
	$locale.NUMBER_FORMATS.DECIMAL_SEP = ","



###

app.run ($rootScope, $location) ->

	$rootScope.$on '$locationChangeSuccess', ->
		$rootScope.actualLocation = $location.path()

	$rootScope.$watch () ->
		return $location.path()
	, (newLocation, oldLocation) ->

		if $rootScope.actualLocation == newLocation

			if $rootScope.player
				$rootScope.player.minimize()

###

