
gulp         = require("gulp")
coffee       = require("gulp-coffee")
stylus       = require("gulp-stylus")
watch        = require("gulp-watch")
livereload   = require("gulp-livereload")
include      = require("gulp-include")
prefix       = require("gulp-autoprefixer")
concat       = require("gulp-concat")
jade         = require("gulp-jade")
addsrc       = require("gulp-add-src")
uglify       = require("gulp-uglify")
csso         = require("gulp-csso")
changed      = require("gulp-changed")

path_dev     = "dev"
path_dist    = "dist"

files =

	jade:
		watch:          path_dev + "/jade/**/*.jade"
		src:            path_dev + "/jade/**/*.jade"
		destDist:       path_dist

	stylus:
		watch:          path_dev + "/stylus/**/*.styl"
		src:            path_dev + "/stylus/*.styl"
		destDist:       path_dist + "/css"

	coffee:
		watch:          path_dev + "/coffee/**/**/*.coffee"
		src:            path_dev + "/coffee/app.coffee"
		destDist:       path_dist + "/js"
		plugins:		[
							"bower_components/angular/angular.min.js"
							"bower_components/angular-ui-router/release/angular-ui-router.min.js"
							"bower_components/ui-router-extras/release/ct-ui-router-extras.min.js"
							"bower_components/angular-animate/angular-animate.min.js"
							"bower_components/angular-sanitize/angular-sanitize.min.js"
							#"bower_components/angular-touch/angular-touch.min.js"
							#"bower_components/angular-swipe/dist/angular-swipe.min.js"
							#"node_modules/angular-fastclick/dist/index.min.js"
							"bower_components/moment/min/moment.min.js"
							"bower_components/moment-range/lib/moment-range.min.js"
							#"bower_components/lodash/dist/lodash.min.js"
							"bower_components/ng-tags-input/ng-tags-input.min.js"
							"bower_components/angular-mighty-datepicker/build/angular-mighty-datepicker.js"
							"bower_components/angular-bindonce/bindonce.min.js"
						]

gulp.task "default", ->

	livereload.listen()
	gulp.watch(path_dist + "/css/*.css").on "change", livereload.changed

	gulp.watch files.jade.watch,        [ "build:html" ]
	gulp.watch files.stylus.watch,      [ "build:css" ]
	gulp.watch files.coffee.watch,      [ "build:js" ]

	return


gulp.task 'build', [
	'build:html'
	'build:js'
	'build:css'
]

gulp.task "build:html", ->
	gulp.src(files.jade.src)
		.pipe(jade(pretty: false))
		.pipe(gulp.dest(files.jade.destDist))
	return

gulp.task "build:css", ->
	gulp.src(files.stylus.src)
		.pipe(stylus({'include css': true}))
		.pipe(prefix())
		.pipe(csso())
		.pipe(gulp.dest(files.stylus.destDist))
	return

gulp.task "build:js", ->
	gulp.src(files.coffee.src)
		.pipe(include())
		.pipe(coffee(bare: true))
		.pipe(addsrc.prepend(files.coffee.plugins))
		.pipe(concat("main.js"))
		#.pipe(uglify())
		.pipe(gulp.dest(files.coffee.destDist))
	return


