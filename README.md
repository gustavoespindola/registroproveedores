ChileCompra
==========

Para comenzar a trabajar en el front-end de este proyecto, es necesario tener instaladas algunas dependencias.

##1. Liberías##

Instalar librería de NodeJS https://nodejs.org/es/

##2. Clonar##

Clonar este repositorio dentro de un servidor local. Las peticiones http a archivos no funcionan bajo el protocolo file://.

##3. Terminal##

En la terminal dentro de la carpeta del proyecto, ejecutar el siguiente comando para instalar las dependencias de node package manager (NPM):

	sudo npm install

Luego, instalar las dependencias de bower:

	bower install

Para comenzar a desarrollar, es necesario ejecutar un listener para automatizar la compilación de archivos coffee/jade/stylus:

	gulp

Debe aparecer un mensaje al tipo:

	Starting 'default'...
	[16:11:30] Finished 'default' after 63 ms
	[16:11:30] Live reload server listening on: 35729

##4. Workflow##

Se debe trabajar sólo en la carpeta `dev`. La carpeta `dist` es sólo para distribucuión con los archivos finales minificados.

Cada vez que se realiza un cambio de en `dev`, se ejecutan diferentes comandos de compilación para archivos JS, HTML y CSS:

	[16:14:04] Starting 'build:js'...
	[16:14:04] Finished 'build:js' after 2.69 ms
	[16:15:36] Starting 'build:html'...
	[16:15:36] Finished 'build:html' after 2.83 ms

Todo el código está modularizado, por lo que cualquier componente se puede encontrar mediante `cmd`+`p` en Sublime Text.


##5. Estructura del proyecto##

+ /coffee
	+ **config**: Archivos de configuración
	+ **controllers**: Controladores sociados a diferentes vistas. Es la lógica escencial de la aplicación.
	+ **directives**: Componentes a los que se les puede inyectar datos, como articles, campos de formularios, o elementos que se repiten constantemente pero bajo diferentes contextos o datos.	
	+ **filter**: Funciones aplicables a cualquier valor dentro de las vistas, como formateo de datos o de fechas.
	+ **services**: Funciones específicas a las que puedo acceder desde un controlador, como llamados a APIs, trackeo de eventos y otros.
+ /jade
	+ **directives**: Templates asociados a cada una de las directivas de angular.
	+ **views**: Templates de cada vista del sitio.
+ /stylus
	+ **core**: Archivos básicos para que funcionen las reglas de CSS, como variables, animaciones, reset y otros.
	+ **modules**: Archivos independientes por cada componente de la interfaz. Todo bajo nomenclaturas BEM.


